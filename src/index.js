import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import Clearpoint from './Clearpoint.js';
import rootReducer from './reducers/rootReducer.js';

import 'bootstrap/dist/css/bootstrap.css';

const store = createStore(rootReducer);

ReactDOM.render(
  <Provider store={store}>
      <Clearpoint />
  </Provider>,
  document.getElementById('clearpoint-test')
);
