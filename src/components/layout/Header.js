import React, { Component } from 'react';
import { connect } from 'react-redux';

function mapStateToProps(state) {
  return {
    companyInfo: state.companyInfo.companyInfo
  };
}

const dateFormat = {
  year: 'numeric',
  month: 'long',
  day: '2-digit'
}

class Header extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { companyInfo } = this.props;
    const companyEst = new Intl.DateTimeFormat('en-GB', dateFormat).format(new Date(companyInfo.companyEst));

    return (  
      <div id="header">
        <h1> {companyInfo.companyName} </h1>
        <div id="company-infos">
          <p id="company-motto"> {companyInfo.companyMotto} </p>
          <p id="company-est">Since {companyEst}</p>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(Header);
