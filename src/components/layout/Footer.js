import React, { Component } from "react";

import './layout.css';

class Footer extends Component {
  render() {
    return (  
      <div id="footer">
        <p> Florian Poyer - florian.poyer@gmail.com - 0415 990 275 </p>
      </div>
    );
  }
}

export default Footer;
