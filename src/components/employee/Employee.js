import React, { Component } from "react";
import { connect } from 'react-redux';
import * as _ from 'lodash';

import './Employee.css';
import ModalEmployee from "./ModalEmployee";
import { TOGGLE_MODAL } from "../../actions/employeeActions";

const mapStateToProps = state => {
  return {
    employees: state.employees.employees,
    employeeSelected: state.employees.employeeSelected
  };
}

const mapDispachToProps = dispach => {
  return {
    toggleModal: (employeeSelected) => dispach({type: TOGGLE_MODAL, employeeSelected}),
  }
}

class Employee extends Component {
  constructor(props) {
    super(props);
    this.state = { employeeInfos: {}, employeesList: [] };
  }

  handleClickEmployee(employee) {
    this.setState({ employeeInfos: employee });
    // Show Modal
    this.props.toggleModal(true);
  }

  sortEmployeesList(employees, event) {
    const field = event.target.value;
    const listSorted = field !== '' ? _.sortBy(employees, e => e[field]) : _.cloneDeep(this.props.employees);
    this.setState({employeesList: listSorted});
  }
  
  filterEmployeesList(event) {
    const word = event.target.value;
    const employees = _.cloneDeep(this.props.employees);

    // Filter by firstname or lastname incensitive case
    const listFiltered = (word !== '' ? 
    _.filter(employees, e => {
      return (
        _.includes(e.firstName.toLowerCase(), word.toLowerCase()) || _.includes(e.lastName.toLowerCase(), word.toLowerCase()));
      })
    : employees);

    this.setState({employeesList: listFiltered});
  }

  render() {
    const { employeeSelected } = this.props;
    const { employeeInfos, employeesList } = this.state;

    return (
      <div>
        <div id="employee-informations" className={employeeSelected ? 'show' : 'hidden'}>
          {employeeSelected ? <ModalEmployee employeeInfos={employeeInfos} /> : ''}
        </div>
        <div id="employees-header" className="row">
          <div className="col-md-6">
            <h3>Our employees</h3>
          </div>
          <div className="col-md-3 filter-fields">  
            <label>Sort by: </label>
            <select onChange={this.sortEmployeesList.bind(this, employeesList)}>
              <option value=""></option>
              <option value="firstName">First Name</option>
              <option value="lastName">Last Name</option>
            </select>
          </div>
          <div className="col-md-3 filter-fields">
            <label>Search</label>
            <input type="text" onChange={this.filterEmployeesList.bind(this)}/>
          </div>
        </div>
        <div id="employees-list" className="row">
          { employeesList.map(employee => {
            return (
              <div className={"col-md-4 col-sm-6 " + (employeeSelected && employeeInfos.id === employee.id ? 'selected' : '')} 
                key={employee.id} onClick={this.handleClickEmployee.bind(this, employee)}>
                <div className="card">
                  <span className="avatar"><img src={employee.avatar} /></span>
                  <span>
                    <b>{employee.firstName} {employee.lastName}</b>
                    <p>{employee.bio}</p>
                  </span>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    );
  }

  componentDidMount() {
    this.setState({employeesList: _.cloneDeep(this.props.employees)});
  }
}

export default connect(mapStateToProps, mapDispachToProps)(Employee);
