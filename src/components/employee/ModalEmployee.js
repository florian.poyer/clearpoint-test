import React, { Component } from "react";
import { connect } from "react-redux";
import { TOGGLE_MODAL } from "../../actions/employeeActions";

import './ModalEmployee.css';

const dateFormat = {
  year: 'numeric',
  month: '2-digit',
  day: '2-digit'
}

const mapStateToProps = state => {
  return {
    employeeSelected: state.employees.employeeSelected
  };
}

const mapDispachToProps = dispach => {
  return {
    toggleModal: (employeeSelected) => dispach({type: TOGGLE_MODAL, employeeSelected}),
  }
}

class ModalEmployee extends Component {
  constructor(props) {
    super(props);

    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.closeModal();
    }
  }

  handleClickCross() {
    this.closeModal();
  }

  closeModal() {
    this.props.toggleModal(false);
  }

  render() {
    const { employeeInfos } = this.props;
    const dateJoined = new Intl.DateTimeFormat('en-GB', dateFormat).format(new Date(employeeInfos.dateJoined));

    return (  
      <div className="template-modal" ref={this.setWrapperRef}>
        <div className="content-modal">
          <span onClick={this.handleClickCross.bind(this)}>X</span>
          <div className="left-info">
            <img src={employeeInfos.avatar} />
            <p><b>Job :</b>{employeeInfos.jobTitle}</p>
            <p><b>Age :</b>{employeeInfos.age}</p>  
            <p><b>Joined in :</b>{dateJoined}</p>  
          </div>
          <div className="right-data">
            <h3>{employeeInfos.firstName} {employeeInfos.lastName}</h3>
            <hr />
            <p>{employeeInfos.bio}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispachToProps)(ModalEmployee);
