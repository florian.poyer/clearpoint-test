export const UPDATE_COMPANY = 'UPDATE_COMPANY';

export const updateCompany = (companyInfo) => ({ type: UPDATE_COMPANY, companyInfo });