export const UPDATE_EMPLOYEES = 'UPDATE_EMPLOYEES';
export const TOGGLE_MODAL = 'TOGGLE_MODAL';

export const updateEmployees = (employees) => ({ type: UPDATE_EMPLOYEES, employees });
export const toggleModal = (employeeSelected) => ({ type: TOGGLE_MODAL, employeeSelected });