import { UPDATE_EMPLOYEES, TOGGLE_MODAL } from '../actions/employeeActions';

const initialEmployee = {
  employees: [],
  employeeSelected: false
}

export default function employeeReducer(state = initialEmployee, action) {
  switch (action.type) {
    case UPDATE_EMPLOYEES:
      return {
        ...state,
        employees: action.employees,
        employeeSelected: false
      };
  
    case TOGGLE_MODAL:
      return {
        ...state,
        employeeSelected: action.employeeSelected
      };

    default:
      return state;
  }
}
