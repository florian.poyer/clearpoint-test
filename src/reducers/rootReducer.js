import { combineReducers } from "redux";
import employees from "./employeeReducer";
import companyInfo from "./companyReducer";

export default combineReducers({
  companyInfo,
  employees
});
