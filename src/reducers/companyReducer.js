import { UPDATE_COMPANY } from '../actions/companyActions';

export default function companyReducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_COMPANY:
      return {
        ...state,
        companyInfo: action.companyInfo
      };
  
    default:
      return state;
  }
}
