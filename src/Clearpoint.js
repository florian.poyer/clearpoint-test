import React, { Component } from "react";
import { connect } from 'react-redux';

// Import actions
import { UPDATE_EMPLOYEES } from './actions/employeeActions';
import { UPDATE_COMPANY } from './actions/companyActions';

// Import components
import Header from './components/layout/Header';
import Footer from './components/layout/Footer';
import Employee from './components/employee/Employee';

import './Clearpoint.css';

function mapStateToProps(state) {

  return {
    companyInfo: state.companyInfo,
    employees: state.employees.employees
  };
}

function mapDispachToProps(dispach) {
  return {
    updateCompany: (companyInfo) => dispach({type: UPDATE_COMPANY, companyInfo}),
    updateEmployees: (employees) => dispach({type: UPDATE_EMPLOYEES, employees})
  } 
}

class Clearpoint extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  render() {
    const { loading } = this.state;

    if(loading) {
      return( <div>Loading...</div> );
    } else {
      return (
        <div>
          <Header />
          <Employee />
          <Footer />
        </div>
      );
    }
  }

  componentDidMount() {
    // Could be in a service file
    fetch('/sample-data.json')
    .then(res => res.json())
    .then(result => {
      this.props.updateCompany(result.companyInfo);
      this.props.updateEmployees(result.employees);
      this.setState({ loading: false });
    });
  }
}

export default connect(mapStateToProps, mapDispachToProps)(Clearpoint);
